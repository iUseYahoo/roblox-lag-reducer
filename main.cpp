// including the needed libraries
#include <iostream>
#include <windows.h>
#include <ResourceIndexer.h>

// the function with all the code
// this function will make the program run as needed
int robloxLagReducer() {
	// getting roblox and its process ID
	HWND hwnd = FindWindowA(NULL, "Roblox");
	DWORD procID;
	GetWindowThreadProcessId(hwnd, &procID);
	HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, procID);

	// Checking for the name of a window called 'Roblox'
	// if the window is not found tell user Roblox isnt found
	if (hwnd == NULL) {
		std::cout << "Cannot find Roblox" << std::endl;
		Sleep(3000);
		exit(-1);
	}
	// tells user that the roblox window was found
	std::cout << "Roblox window was found." << std::endl;

	// if roblox is found check for its procID aka Process ID
	// if the roblox procID is NULL then tell the user that the program cannot get roblox's procID
	else {
		if (procID == NULL) {
			std::cout << "Cannot get Roblox's Process ID." << std::endl;
			Sleep(3000);
			exit(-1);
		}
		else {
			// tells user that the roblox process id was found
			std::cout << "Roblox Process ID was found." << std::endl;

			/*
			High
			Above Normal
			Normal
			Below Normal
			*/
			// Number Selector used for picking the priority level of roblox
			int PriorityLevel;

			// tells the user the order of the priority levels
			std::cout << "Reminder: If you have a low end system, I recommend not setting it to 'High'. This will lag your system when you try to do other things outside Roblox while Roblox is set to High." << std::endl;
			std::cout << "The following Application Priority Levels are sorted at < Higher - Lower>" << std::endl;
			// tells the users all the priorities they can set roblox to
			std::cout << "1 - High | 2 - Above Normal | 3 - Normal | 4 - Below Normal" << std::endl;
			std::cout << "Select a level via its number => ";
			// get the number input for the the priority
			std::cin >> PriorityLevel;

			if (PriorityLevel == 1) {
				// changes robloxs priroty to high 
				SetPriorityClass(handle, HIGH_PRIORITY_CLASS);
				std::cout << "RobloxPlayerBeta.exe's Application Priority was set to 'High'." << std::endl;
			}
			else if (PriorityLevel == 2) {
				// changes robloxs priroty to above normal 
				SetPriorityClass(handle, ABOVE_NORMAL_PRIORITY_CLASS);
				std::cout << "RobloxPlayerBeta.exe's Application Priority was set to 'Above Normal'." << std::endl;
			}
			else if (PriorityLevel == 3) {
				// changes robloxs priroty to normal 
				SetPriorityClass(handle, NORMAL_PRIORITY_CLASS);
				std::cout << "RobloxPlayerBeta.exe's Application Priority was set to 'Normal'." << std::endl;
			}
			else if (PriorityLevel == 4) {
				// changes robloxs priroty to below normal 
				SetPriorityClass(handle, BELOW_NORMAL_PRIORITY_CLASS);
				std::cout << "RobloxPlayerBeta.exe's Application Priority was set to 'Below Normal'." << std::endl;
			}
			else {
				// lets the user know that their option doesnt exist
				std::cout << PriorityLevel << " is not a valid option." << std::endl;
				// waits 2 and a half seconds after the first message
				Sleep(2500);
				// lets the user know that the program will exit
				std::cout << "Exiting." << std::endl;
				// lets the user read the message for 1 second before exiting
				Sleep(1000);
				// exits the program
				exit(-1);
			}

			// return false read about it from
			// https://www.geeksforgeeks.org/return-0-vs-return-1-in-c/#:~:text=return%200%3A%20A%20return%200,it%20was%20intended%20to%20do.
			return 0;
		}
	}
};

// the function that will start at the start of the program starting
int main() {
	// runs the main code Function called robloxLagReducer
	robloxLagReducer();
};
