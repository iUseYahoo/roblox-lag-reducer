# Roblox Lag Reducer
- You do not need to have [Visual Studio](https://visualstudio.microsoft.com) installed.
- This is not a tool like Roblox FPS Unlocker this is a project to somewhat grasp the concept of C++ and learn from this.

# Visual Studio / File Help
- If you cannot find the project files it is because GitLab had replaced the spaces in the filename with an '_'. You can replace the '_' (underscore) with a space as it shoudlve been.

- If Visual Studio says that your currentl version of Visual Studio isnt the same as the one the project was built in (VS2019), Open the error output / console in Visual Studio and follow its instructions to fix it. To fix it should take only a couple of clicks.

# How to make the file
- Note you can either build the file or download it from the [repository](https://gitlab.com/iUseYahoo/roblox-lag-reducer/-/blob/main/Roblox_Lag_Reducer.exe) itself. (64-Bit Application).
```
- Step 1: Download Visual Studio and install the C++ installtion option.
- Step 2: Open the downloaded folder where the ".sln" file is and double click it.
- Step 3: When you open the ".sln" file you will see at the top "Debug" or "Start", Click that. (Should be a green sideways triangle)
- Step 4: Check the build output and find the file output path.
- Step 5: When you get in to the output path open the "Debug" or "Release" folder, In there should be the .exe which you can now run.
```

# FAQ
- What Visual Studio are you using? I am using Visual Studio 2019. (Personal Preference, It is what I learned on. Does not make a difference)
- Will this work on my Computer / Laptop? The answer is for most people yes. If you're on a 64 Bit Windows OS you will not need to change anything when building the app. If you're on a 32 Bit Windows OS you will need to change the application settings to work on your machine. [this](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjJx6aC8ez0AhVCUGwGHf9UD3oQFnoECAgQAQ&url=https%3A%2F%2Fdocs.microsoft.com%2Fen-us%2Fvisualstudio%2Fide%2Fhow-to-configure-projects-to-target-platforms&usg=AOvVaw1tpvh3KTgub73dXuk_Z2QB) guide will show you how to do so.

# Contact
- For help message me on [Discord](https://discord.com/users/732971774140088320).
